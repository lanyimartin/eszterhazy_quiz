import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import axios from "axios";
import firebase from "firebase";

Vue.prototype.$axios = axios;
Vue.config.productionTip = false;

const firebaseConfig = {
  apiKey: "AIzaSyD4oXZ0GyUisiUYBQPzKmFId7gCTsMDf_0",
  authDomain: "eszterhazy-honfoglalo.firebaseapp.com",
  databaseURL: "https://eszterhazy-honfoglalo-default-rtdb.firebaseio.com",
  projectId: "eszterhazy-honfoglalo",
  storageBucket: "eszterhazy-honfoglalo.appspot.com",
  messagingSenderId: "203631903689",
  appId: "1:203631903689:web:56b7cb2d5e2ae2e0169e18",
  measurementId: "G-EBL1G8GHSG"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let app;
firebase.auth().onAuthStateChanged(user => {
  console.log(user);
  if(!app){
    app = new Vue({router,store,render: h => h(App)}).$mount("#app");
  }
})

